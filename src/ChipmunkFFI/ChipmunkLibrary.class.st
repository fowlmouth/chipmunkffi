Class {
	#name : #ChipmunkLibrary,
	#superclass : #FFILibrary,
	#category : #ChipmunkFFI
}

{ #category : #'accessing platform' }
ChipmunkLibrary >> macLibraryName [ 
	^ 'libchipmunk.7.dylib'
]
