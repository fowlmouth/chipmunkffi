"
A ChipmunkSpaceTest is a test class for testing the behavior of ChipmunkSpace
"
Class {
	#name : #ChipmunkSpaceTest,
	#superclass : #TestCase,
	#instVars : [
		'space'
	],
	#category : #'ChipmunkFFI-Tests'
}

{ #category : #running }
ChipmunkSpaceTest >> setUp [
	space := ChipmunkSpace new
]

{ #category : #test }
ChipmunkSpaceTest >> testGravity [
	|vec|
	vec := ChipmunkVector x: 1.0 y: 1.0.
	space gravity: vec.
	self assert: space gravity equals: vec
]
