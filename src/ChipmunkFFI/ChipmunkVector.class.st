Class {
	#name : #ChipmunkVector,
	#superclass : #FFIStructure,
	#classVars : [
		'OFFSET_X',
		'OFFSET_Y'
	],
	#pools : [
		'ChipmunkTypes'
	],
	#category : #ChipmunkFFI
}

{ #category : #'class initialization' }
ChipmunkVector class >> fieldsDesc [ 
	^ #(
		cpFloat x;
		cpFloat y;
	)
]

{ #category : #'class initialization' }
ChipmunkVector class >> initialize [ 
	self rebuildFieldAccessors 
]

{ #category : #'instance creation' }
ChipmunkVector class >> x: x y: y [
	^ self new x: x; y: y; yourself
]

{ #category : #'instance creation' }
ChipmunkVector class >> zero [
	^ self x: 0 y: 0 
]

{ #category : #'accessing structure variables' }
ChipmunkVector >> x [
	"This method was automatically generated"
	^handle doubleAt: OFFSET_X
]

{ #category : #'accessing structure variables' }
ChipmunkVector >> x: anObject [
	"This method was automatically generated"
	handle doubleAt: OFFSET_X put: anObject
]

{ #category : #'accessing structure variables' }
ChipmunkVector >> y [
	"This method was automatically generated"
	^handle doubleAt: OFFSET_Y
]

{ #category : #'accessing structure variables' }
ChipmunkVector >> y: anObject [
	"This method was automatically generated"
	handle doubleAt: OFFSET_Y put: anObject
]
