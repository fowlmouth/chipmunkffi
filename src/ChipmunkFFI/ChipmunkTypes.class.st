Class {
	#name : #ChipmunkTypes,
	#superclass : #Object,
	#classVars : [
		'CpLibrary',
		'cpFloat',
		'cpSpace',
		'cpVect'
	],
	#category : #ChipmunkFFI
}

{ #category : #'class initialization' }
ChipmunkTypes class >> initialize [
	CpLibrary := ChipmunkLibrary.
	cpFloat := #double.
	cpVect := ChipmunkVector.
	cpSpace := ChipmunkSpace.
]
