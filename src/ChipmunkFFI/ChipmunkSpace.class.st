Class {
	#name : #ChipmunkSpace,
	#superclass : #FFIOpaqueObject,
	#pools : [
		'ChipmunkTypes'
	],
	#category : #ChipmunkFFI
}

{ #category : #'instance creation' }
ChipmunkSpace class >> new [
	^ self ffiCall: #( ChipmunkSpace* cpSpaceNew() ) library: CpLibrary
]

{ #category : #'as yet unclassified' }
ChipmunkSpace >> gravity [
	^ self ffiCall: #( ChipmunkVector cpSpaceGetGravity(self) ) library: CpLibrary
]

{ #category : #'as yet unclassified' }
ChipmunkSpace >> gravity: gravity [
	^ self ffiCall: #( void cpSpaceSetGravity(self, ChipmunkVector gravity) ) library: CpLibrary
]
